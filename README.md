# strutil

strutil est une librairie Go pour manipuler des chaînes de caractères.

Elle se décompose en différents modules :

- align: fournit des fonctions pour aligner une chaîne de caractères sur une ligne
- compare: fournit des fonctions de comparaison avancées de chaînes de caractères
- encrypt: fournit des fonctions pour chiffrer des données
- randomize: fournit une fonction pour générer des chaînes aléatoires
- style: fournit des fonctions pour appliquer des styles et des coulours à une chaîne en sortie de commande
- unidecode: fournit des fonctions pour convertir tous les caractères d’une chaîne en caractères ASCII équivalents
