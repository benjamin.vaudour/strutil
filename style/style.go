package style

import (
	"fmt"
)

type Style int

const (
	Normal    Style = 0
	Bold      Style = 1
	Italic    Style = 3
	Underline Style = 4
	Blink     Style = 5
	Inverted  Style = 7
	Crossed   Style = 9
	Cancel    Style = 20
)

var styles = map[string]Style{
	"normal":     Normal,
	"bold":       Bold,
	"italic":     Italic,
	"underline":  Underline,
	"blink":      Blink,
	"inverted":   Inverted,
	"crossed":    Crossed,
	"!bold":      Cancel + Bold,
	"!italic":    Cancel + Italic,
	"!underline": Cancel + Underline,
	"!blink":     Cancel + Blink,
	"!inverted":  Cancel + Inverted,
	"!crossed":   Cancel + Crossed,
}

func (st Style) IsCancel() bool {
	return st >= 20
}

func (st Style) String() string {
	return fmt.Sprintf("%d", st)
}

func StyleOf(name string) Style {
	if st, ok := styles[name]; ok {
		return st
	}
	return -1
}
