package style

import (
	"fmt"
	"strings"
)

type Color int

const (
	Black Color = iota
	Red
	Green
	Yellow
	Blue
	Majenta
	Cyan
	White

	Light      Color = 1 << 3
	Background Color = 1 << 4
	None       Color = -1
)

var colors = map[string]Color{
	"black":   Black,
	"red":     Red,
	"green":   Green,
	"yellow":  Yellow,
	"blue":    Blue,
	"majenta": Majenta,
	"cyan":    Cyan,
	"white":   White,
}

var prefixes = []string{
	"fg_l_",
	"fg_",
	"l_",
	"bg_l_",
	"bg_",
}

var prefixFuncs = map[string]func(string) Color{
	"fg_l_": FgLightColor,
	"fg_":   FgDarkColor,
	"l_":    FgLightColor,
	"bg_l_": BgLightColor,
	"bg_":   BgDarkColor,
}

func (c Color) IsLight() bool {
	return c&Light != 0
}

func (c Color) IsBackground() bool {
	return c&Background != 0
}

func (c Color) Base() int {
	if c < 0 {
		return -1
	}
	b := 30 + (c & White)
	if c.IsBackground() {
		b += 10
	}
	if c.IsLight() {
		b += 60
	}
	return int(b)
}

func (c Color) String(force ...bool) string {
	b := c.Base()
	if b < 0 {
		return ""
	}
	s := fmt.Sprint(b)
	if len(force) > 0 && force[0] && !c.IsLight() && !c.IsBackground() {
		s = "2;" + s
	}
	return s
}

func FgDarkColor(name string) Color {
	if c, ok := colors[name]; ok {
		return c
	}
	return -1
}

func FgLightColor(name string) Color {
	if c, ok := colors[name]; ok {
		return c | Light
	}
	return -1
}

func FgColor(name string) Color {
	if strings.HasPrefix(name, "l_") {
		n := strings.TrimPrefix(name, "l_")
		return FgLightColor(n)
	}
	return FgDarkColor(name)
}

func BgDarkColor(name string) Color {
	if c, ok := colors[name]; ok {
		return c | Background
	}
	return -1
}

func BgLightColor(name string) Color {
	if c, ok := colors[name]; ok {
		return c | Light | Background
	}
	return -1
}

func BgColor(name string) Color {
	if strings.HasPrefix(name, "l_") {
		n := strings.TrimPrefix(name, "l_")
		return BgLightColor(n)
	}
	return BgDarkColor(name)
}

func ColorOf(name string) Color {
	for _, prefix := range prefixes {
		if strings.HasPrefix(name, prefix) {
			n := strings.TrimPrefix(name, prefix)
			return prefixFuncs[prefix](n)
		}
	}
	return FgDarkColor(name)
}
