package style

import (
	"fmt"
	"strings"
)

type Formatter struct {
	fg     Color
	bg     Color
	styles []Style
}

func (f *Formatter) Background() Color {
	return f.bg
}

func (f *Formatter) Foreground() Color {
	return f.fg
}

func (f *Formatter) Styles() []Style {
	out := make([]Style, len(f.styles))
	copy(out, f.styles)
	return out
}

func (f *Formatter) AddBg(name string) *Formatter {
	c := BgColor(name)
	if c < 0 {
		c = ColorOf(name)
		if !c.IsBackground() {
			c = -1
		}
	}
	f.bg = c
	return f
}

func (f *Formatter) RemoveBg() *Formatter {
	f.bg = -1
	return f
}

func (f *Formatter) AddFg(name string) *Formatter {
	c := FgColor(name)
	if c < 0 {
		c = ColorOf(name)
		if c.IsBackground() {
			c = -1
		}
	}
	f.fg = c
	return f
}

func (f *Formatter) RemoveFg() *Formatter {
	f.fg = -1
	return f
}

func (f *Formatter) AddColor(name string) *Formatter {
	c := ColorOf(name)
	if c >= 0 {
		if c.IsBackground() {
			f.bg = c
		} else {
			f.fg = c
		}
	}
	return f
}

func (f *Formatter) AddStyle(name string) *Formatter {
	st := StyleOf(name)
	if st >= 0 {
		f.styles = append(f.styles, st)
	}
	return f
}

func (f *Formatter) RemoveStyle(name string) *Formatter {
	st := StyleOf(name)
	if st >= 0 {
		if st.IsCancel() {
			st -= 20
		} else {
			st += 20
		}
		f.styles = append(f.styles, st)
	}
	return f
}

func (f *Formatter) Add(formats ...string) *Formatter {
	for _, n := range formats {
		if StyleOf(n) < 0 {
			f.AddColor(n)
		} else {
			f.AddStyle(n)
		}
	}
	return f
}

func (f *Formatter) Reset() *Formatter {
	f.fg = -1
	f.bg = -1
	f.styles = []Style{}
	return f
}

func (f *Formatter) String() string {
	var args []string
	var is_bold bool
	for _, s := range f.styles {
		args = append(args, s.String())
		switch s {
		case Bold:
			is_bold = true
		case Bold | Cancel, Normal:
			is_bold = false
		}
	}
	if f.fg >= 0 {
		args = append(args, f.fg.String(is_bold))
	}
	if f.bg >= 0 {
		args = append(args, f.bg.String())
	}
	return fmt.Sprintf("\033[%sm", strings.Join(args, ";"))
}

func (f *Formatter) Format(str string) string {
	return f.String() + str + "\033[m"
}

func New(formats ...string) *Formatter {
	f := new(Formatter)
	return f.Reset().Add(formats...)
}

func Format(str string, formats ...string) string {
	f := New(formats...)
	return f.Format(str)
}
