package encrypt

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"hash"

	"framagit.org/benjamin.vaudour/strutil/randomize"
)

// Liste des fonctions de Hash supportées
var (
	encoder = map[string]func() hash.Hash{
		"md5":    md5.New,
		"sha1":   sha1.New,
		"sha256": sha256.New,
		"sha512": sha512.New,
	}
)

//AvailableEncoders retourne la liste des noms des hashes supportés
func AvailableEncoders() []string {
	var enc []string
	for e := range encoder {
		enc = append(enc, e)
	}
	return enc
}

//IsEncoderAvailable vérifie si le nom fournit en entrée fait partie des hashes supportés
func IsEncoderAvailable(enc string) bool {
	_, exists := encoder[enc]
	return exists
}

//Encode chiffre la donnée d‘entrée en utilisant l’algorithme demandé
func Encode(data string, algorithm string) string {
	var encoded []byte
	if f, ok := encoder[algorithm]; ok {
		hash := f()
		hash.Write([]byte(data))
		encoded = hash.Sum(nil)
		return fmt.Sprintf("%x", encoded)
	}
	return data
}

//EncodePassword chiffre le mot de passe donné en entrée avec l’algorithme demandé
//Si une longueur de sel est définie, la fonction retourne également le sel (généré aléatoirement) utilisé pour le chiffrage.
func EncodePassword(plainpassword, algorithm string, saltLen ...int) (encodedPassword, generatedSalt string) {
	if len(saltLen) > 0 {
		generatedSalt = randomize.String(saltLen[0])
	}
	encodedPassword = Encode(plainpassword+generatedSalt, algorithm)
	return
}
