package randomize

//Le package randomize implémente des fonctions de randomization sur les strings

import (
	"math/rand"
	"time"
)

//String retourne une chaîne aléatoire de taille size et contenant uniquement des caractères donnés en option
//Si aucun caractère n’est donné, la chaîne ne contiendra que des caractères de la classe [A-Za-z0-9]
func String(size int, choices ...string) string {
	if len(choices) == 0 {
		choices = []string{"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"}
	}
	chars := []rune(choices[0])
	lc := len(chars)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	result := make([]rune, size)
	for i := range result {
		result[i] = chars[r.Intn(lc)]
	}
	return string(result)
}
