package align

import (
	"strings"
	"unicode/utf8"
)

//Tab crée une tabulation d’une longueur donnée
func Tab(size int) string {
	return strings.Repeat(" ", size)
}

//TabulateLeft ajoute une tabulation gauche à une chaîne donnée
func TabulateLeft(str string, tabSize int) string {
	return Tab(tabSize) + str
}

//TabulateRight ajoute une tabulation droite à une chaîne donnée
func TabulateRight(str string, tabSize int) string {
	return str + Tab(tabSize)
}

func Tabulate(str string, leftSize, rightSize int) string {
	return Tab(leftSize) + str + Tab(rightSize)
}

type AlignmentType int

const (
	LEFT AlignmentType = iota
	RIGHT
	CENTER
)

type Alignment struct {
	t AlignmentType
	l int
}

func New(t AlignmentType, size int) Alignment {
	if size <= 0 {
		panic("Size of alignment must be positive")
	}
	return Alignment{
		t: t,
		l: size,
	}
}

func _s(str string) int { return utf8.RuneCountInString(str) }

func (a Alignment) Format(str string) string {
	l := _s(str)
	if l == a.l {
		return str
	}
	d := a.l - l
	runes := []rune(str)
	switch a.t {
	case RIGHT:
		if d > 0 {
			return TabulateLeft(str, d)
		}
		return string(runes[-d:])
	case CENTER:
		if d > 0 {
			left := d >> 1
			right := d - left
			return Tabulate(str, left, right)
		}
		left := (-d) >> 1
		return string(runes[left : left+a.l])
	default:
		if d > 0 {
			return TabulateRight(str, d)
		}
		return string(runes[:a.l])
	}
}

func Align(str string, align AlignmentType, size int) string {
	a := New(align, size)
	return a.Format(str)
}

func Left(str string, size int) string {
	return Align(str, LEFT, size)
}

func Right(str string, size int) string {
	return Align(str, RIGHT, size)
}

func Center(str string, size int) string {
	return Align(str, CENTER, size)
}

type ColumnSet []Alignment

func NewColumnSet(aligns ...Alignment) ColumnSet {
	return aligns
}

func (cs ColumnSet) Normalize(columns []string) []string {
	if len(cs) != len(columns) {
		panic("Number of columns must be equal to number of column set")
	}
	out := make([]string, len(columns))
	for i, a := range cs {
		out[i] = a.Format(columns[i])
	}
	return out
}

func (cs ColumnSet) Line(columns []string, sep, prefix, suffix string) string {
	out := cs.Normalize(columns)
	if prefix != "" || suffix != "" {
		for i, c := range out {
			out[i] = prefix + c + suffix
		}
	}
	return strings.Join(out, sep)
}
