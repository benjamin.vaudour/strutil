// Unidecode implements a unicode transliterator, which
// replaces non-ASCII characters with their ASCII
// counterparts

package unidecode

import (
	"strings"
	"unicode"

	"framagit.org/benjamin.vaudour/strutil/unidecode/table"
)

func String(s string) string {
	var ret strings.Builder
	for _, r := range s {
		if r < unicode.MaxASCII {
			ret.WriteRune(r)
			continue
		}
		if r > 0xeffff {
			continue
		}

		section := r >> 8   // Chop off the last two hex digits
		position := r & 255 // Last two hex digits
		if tb, ok := table.Tables[section]; ok {
			if len(tb) > int(position) {
				ret.WriteString(tb[position])
			}
		}
	}
	return ret.String()
}

func ToLower(s string) string {
	return strings.ToLower(String(s))
}

func ToUpper(s string) string {
	return strings.ToUpper(String(s))
}
